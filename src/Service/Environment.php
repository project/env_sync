<?php

namespace Drupal\env_sync\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class Environment.
 *
 * @package Drupal\env_sync
 */
class Environment implements EnvironmentInterface {

  use StringTranslationTrait;

  /**
   * @var array
   */
  protected $settings;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateService;

  /**
   * @var array
   */
  protected $environments;

  /**
   * Constructor.
   */
  public function __construct(Settings $settings, ConfigFactoryInterface $config_factory, StateInterface $state_service, ConfigManagerInterface $config_manager) {
    $this->settings = $settings;
    $this->configFactory = $config_factory;
    $this->stateService = $state_service;
    $this->configManager = $config_manager;
    $this->config = $this->configFactory->getEditable(self::ENV_SYNC_ENVIRONMENT_CONFIG);
    $this->environments = $this->config->get('environment');
  }

  /**
   *
   * @inheritdoc
   *
   **/
  public function add(string $env_name) : bool {
    if (preg_match('#[^a-zA-Z0-9\-\_]#', $env_name)) {
      throw new \Exception($this->t('Use only alphanum, "-" and "_" for environment name.'));
    }

    if(isset($this->environments[$env_name])) {
      throw new \Exception($this->t('This environment already exists (@env_name).', [
        '@env_name' => $env_name
      ]));
    }
    $this->environments[$env_name] = [
      self::ENV_NAME => $env_name,
      self::ENV_CONFIG => [],
      self::ENV_IS_BASE => FALSE,
    ];
    $this->saveEnvironments();
    return TRUE;
  }

  /**
   *
   * @inheritdoc
   *
   **/
  public function remove(string $env_name) : bool {
    $this->environmentExists($env_name);
    unset($this->environments[$env_name]);
    $this->saveEnvironments();
    return TRUE;
  }

  /**
   * @inheritdoc
   **/
  public function getBaseEnvironment() : array {
    $conf = [];
    if (!empty($this->environments)) {
      foreach($this->environments as $name => $env_config) {
        if ($env_config[self::ENV_IS_BASE] === TRUE) {
          $conf = $this->environments[$name];
        }
      }
    }
    return $conf;
  }

  /**
   * @inheritdoc
   **/
  public function getBaseEnvironmentName() : string {
    $name = '';
    $configuration = $this->getBaseEnvironment();
    if (!empty($configuration)) {
      $name = $configuration[self::ENV_NAME];
    }
    return $name;
  }

  /**
   * @inheritdoc
   **/
  public function setBaseEnvironment(string $env_name) : bool {
    $this->environmentExists($env_name);
    foreach($this->environments as $name => $env_config) {
      $this->environments[$name][self::ENV_IS_BASE] = FALSE;
      if ($name === $env_name) {
        $this->environments[$name][self::ENV_IS_BASE] = TRUE;
      }
    }
    $this->saveEnvironments();
    return TRUE;
  }

  /**
   * @inheritdoc
   **/
  public function setCurrentEnvironment(string $env_name) : bool {
    $this->environmentExists($env_name);
    $this->stateService->set(self::STATE_CURRENT_ENV, $env_name);
    return TRUE;
  }

  /**
   * @inheritdoc
   **/
  public function getCurrentEnvironment() : string {
    $current_env_name = $this->stateService->get(self::STATE_CURRENT_ENV, '');
    if (empty($current_env_name)) {
      $current_env_name = $this->settings->get(self::STATE_CURRENT_ENV, '');
    }
    return $current_env_name;
  }

  /**
   * @inheritdoc
   **/
  function removeCurrentEnvironment() : bool {
    $current_env_settings = $this->settings->get(self::STATE_CURRENT_ENV);
    $current_env = $this->stateService->get(self::STATE_CURRENT_ENV);
    if (empty($current_env)) {
      $message = 'Current environment has not been defined in state.';
      if (!empty($current_env_settings)) {
        $message .= ' The current environment "@env_name" is define in settings.php.';
      }
      throw new \Exception($this->t($message, [
        '@env_name' => $current_env_settings
      ]));
    }
    else {
      $this->stateService->delete(self::STATE_CURRENT_ENV);
    }
    return TRUE;
  }

  /**
   * @inheritdoc
   **/
  public function isTheBaseEnvironment(string $env_name) : bool {
    $is_base = FALSE;
    if (isset($this->environments[$env_name])) {
      $is_base = $this->environments[$env_name][self::ENV_IS_BASE];
    }
    return $is_base;
  }


  /**
   * @inheritdoc
   **/
  public function list() : array {
    $env_list = [];
    if (!empty($this->environments)) {
      $env_list = array_keys($this->environments);
      natcasesort($env_list);
    }
    return $env_list;
  }

  /**
   * @inheritdoc
   **/
  public function getAll() : array {
    $env_list = [];
    if (!empty($this->environments)) {
      $env_list = $this->environments;
    }
    return $env_list;
  }

  /**
   * @inheritdoc
   **/
  public function setConfiguration($config_name, $env_name, $force = FALSE) : bool {
    $this->environmentExists($env_name);
    $saved = FALSE;
    if (!$this->configurationExist($config_name, $env_name)) {
      if ($force) {
        $this->environments[$env_name][self::ENV_CONFIG][] = $config_name;
        natcasesort($this->environments[$env_name][self::ENV_CONFIG]);
        $this->saveEnvironments();
        $saved = TRUE;
      }
      else {
        $config = $this->configFactory->get($config_name);
        // Check the configuration is existing in Drupal configurations.
        if (!$config->isNew()) {
          $this->environments[$env_name][self::ENV_CONFIG][] = $config_name;
          natcasesort($this->environments[$env_name][self::ENV_CONFIG]);
          $this->saveEnvironments();
          $saved = TRUE;
        }
      }
    }
    return $saved;
  }

  /**
   * @inheritdoc
   **/
  public function unsetConfiguration($config_name, $env_name) : bool {
    $this->environmentExists($env_name);
    $saved = FALSE;
    if ($this->configurationExist($config_name, $env_name)) {
      $config_key = array_search($config_name, $this->environments[$env_name][self::ENV_CONFIG]);
      if ($config_key !== FALSE) {
        unset($this->environments[$env_name][self::ENV_CONFIG][$config_key]);
        $this->saveEnvironments();
        $saved = TRUE;
      }
    }
    return $saved;
  }

  /**
   * @inheritdoc
   **/
  public function listConfiguration($env_name) : array {
    $this->environmentExists($env_name);
    $configurations = $this->environments[$env_name][self::ENV_CONFIG];
    natcasesort($configurations);
    return $configurations;
  }

  /**
   * @inheritdoc
   **/
  public function configurationExist($config_name, $env_name) : bool {
    $this->environmentExists($env_name);
    $config_exists = FALSE;
    $configurations = $this->environments[$env_name][self::ENV_CONFIG];
      if (!empty($configurations) && in_array($config_name, $configurations)) {
        $config_exists = TRUE;
      }
      return $config_exists;
  }

  protected function saveEnvironments() {
    ksort($this->environments);
    $this->config->set('environment', $this->environments);
    $this->config->save();
  }

  /**
   * Check if an environment is existing.
   *
   * @param string $env_name
   *    The environment name to check.
   *
   * @return bool
   *    Return TRUE if the environment is existing.
   *
   * @throws \Exception
   *    Is throws if the environment does not exist.
   */
  public function environmentExists(string $env_name) : bool {
    if(empty($env_name)) {
      throw new \Exception($this->t('No environment has been found.'));
    }

    if(!isset($this->environments[$env_name])) {
      throw new \Exception($this->t('This environment does not exist (@env_name).', [
        '@env_name' => $env_name
      ]));
    }
    return TRUE;
  }
}
