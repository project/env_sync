<?php

namespace Drupal\env_sync\Service;

/**
 * Interface EnvironmentInterface.
 *
 * @package Drupal\env_sync
 */
Interface EnvironmentInterface {

  const ENV_SYNC_ENVIRONMENT_CONFIG = 'env_sync.environments';

  const ENV_NAME = 'name';
  const ENV_CONFIG = 'config';
  const ENV_IS_BASE = 'is_base_environment';
  const STATE_CURRENT_ENV = 'env_sync.current_environment';

  /**
   * Add an environment.
   *
   * @param string $env_name
   *    Name of the environment.
   *
   * @return bool
   *    TRUE if created.
   */
  public function add(string $env_name) : bool;

  /**
   * Remove an environment.
   *
   * @param string $env_name
   *    Name of the environment.
   *
   * @return bool
   *    TRUE if removed.
   */
  function remove(string $env_name) : bool;

  /**
   *  Get the base environment configuration.
   *
   * @return array
   *    The current base environment configuration.
   */
  function getBaseEnvironment() : array;

  /**
   *  Get the base environment name.
   *
   * @return array
   *    The current base environment configuration.
   */
  function getBaseEnvironmentName() : string;

  /**
   * Set a base environment.
   *
   * @param string $env_name
   *    Name of the environment.
   *
   * @return bool
   *    TRUE if set.
   */
  function setBaseEnvironment(string $env_name) : bool;

  /**
   * Set the current environment.
   *
   * @param string $env_name
   *    Name of the environment.
   *
   * @return bool
   *    TRUE if set.
   */
  function setCurrentEnvironment(string $env_name) : bool;

  /**
   * Get the current environment.
   *
   * @return string
   *    Name of the environment.
   */
  function getCurrentEnvironment() : string;

  /**
   * Remove the current environment (stored in state).
   *
   * @return bool
   *    TRUE if set.
   */
  function removeCurrentEnvironment() : bool;

  /**
   *  Wether or not it's the base environment.
   *
   * @param string $env_name
   *    Name of the environment.
   *
   * @return bool
   *    TRUE if it's the base environment, FALSE otherwise.
   */
  function isTheBaseEnvironment(string $env_name) : bool;

  /**
   * Get all environments data.
   *
   * @return array
   *    List of all environments.
   */
  function getAll() : array;

  /**
   * List all environments available.
   *
   * @return array
   *    List of environments.
   */
  public function list() : array;

  /**
   * Set a configuration into an environment.
   *
   * @param string $config_name
   *    Name of the configuration.
   * @param string $env_name
   *    Name of the environment.
   * @param boolean $force
   *    TRUE to save an non existing configuration.
   *
   * @return boolean
   *    TRUE if the configuration has been set, FALSE otherwise.
   */
  function setConfiguration($config_name, $env_name, $force = FALSE) : bool;

  /**
   * Unset a configuration into an environment.
   *
   * @param string $config_name
   *    Name of the configuration.
   * @param string $env_name
   *    Name of the environment.
   *
   * @return boolean
   *    TRUE if the configuration has been unset, FALSE otherwise.
   */
  function unsetConfiguration($config_name, $env_name) : bool;

  /**
   * List all configuration set into an environment.
   *
   * @param string $env_name
   *    Name of the environment.
   *
   * @return array
   *    Array of configurations name.
   */
  function listConfiguration($env_name) : array;

  /**
   * Check if a configuration is already set in a given environment.
   *
   * @param string $config_name
   *    Name of the configuration.
   * @param string $env_name
   *    Name of the environment.
   *
   * @return boolean
   *    TRUE if the configuration is existing.
   */
  function configurationExist($config_name, $env_name) : bool;

}
