<?php

namespace Drupal\env_sync\Service;

use Drupal\Core\Config\StorageComparer;

/**
 * Interface SynchronizationInterface.
 *
 * @package Drupal\env_sync
 */
Interface SynchronizationInterface {

  /**
   * Import configurations to database, based on base and current environment.
   *
   * @return array
   *    The list of changes.
   */
  public function import() : array;

  /**
   * Export configurations from database to correct environment directory.
   *
   * @return array
   *    The list of changes.
   */
  public function export() : array;

  /**
   * Get information on changes between database and configuration files in environment folders.
   *
   * @param bool $import
   *    TRUE to see information on import process, FALSE to see on export process.
   * @return array
   *    The list of changes.
   */
  public function getChangeList($import = TRUE) : array;

}
