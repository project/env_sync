<?php

namespace Drupal\env_sync\Service;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Synchronization.
 *
 * @package Drupal\env_sync
 */
class Synchronization implements SynchronizationInterface {

  use StringTranslationTrait;

  /**
   * Drupal Settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Drupal File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Environment Service.
   *
   * @var \Drupal\env_sync\Service\EnvironmentInterface
   */
  protected $environmentService;

  /**
   * The synchronization folder defined in settings.php.
   *
   * @var string
   */
  protected $syncFolder;

  /**
   * Name of the current environment.
   *
   * @var string
   */
  protected $currentEnvironmentName;

  /**
   * Name of the base environment.
   *
   * @var string
   */
  protected $baseEnvironmentName;

  /**
   * Drupal config manager service.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Drupal config storage service.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  // Only for import requirements
  protected $eventDispatcher;
  protected $persistentLockBackend;
  protected $typedConfigManager;
  protected $moduleHandler;
  protected $moduleInstaller;
  protected $themeHandler;
  protected $stringTranslationService;
  protected $moduleExtensionList;

  /**
   * Constructor.
   */
  public function __construct(
    Settings $settings,
    FileSystemInterface $file_system,
    EnvironmentInterface $environment_service,
    ConfigManagerInterface $config_manager,
    StorageInterface $config_storage,

  // Only for import requirements
    EventDispatcherInterface $event_dispatcher,
    LockBackendInterface $persistent_lock_backend,
    TypedConfigManagerInterface $typed_config_manager,
    ModuleHandlerInterface $module_handler,
    ModuleInstallerInterface $module_installer,
    ThemeHandlerInterface $theme_handler,
    TranslationInterface $string_translation_service,
    ModuleExtensionList $module_extension_list

  ) {
    $this->settings = $settings;
    $this->fileSystem = $file_system;
    $this->environmentService = $environment_service;
    $this->configManager = $config_manager;
    $this->configStorage = $config_storage;

    // Only for import requirements
    $this->eventDispatcher = $event_dispatcher;
    $this->persistentLockBackend = $persistent_lock_backend;
    $this->typedConfigManager = $typed_config_manager;
    $this->moduleHandler = $module_handler;
    $this->moduleInstaller = $module_installer;
    $this->themeHandler = $theme_handler;
    $this->stringTranslationService = $string_translation_service;
    $this->moduleExtensionList = $module_extension_list;

    $this->currentEnvironmentName = $this->environmentService->getCurrentEnvironment();
    $this->baseEnvironmentName = $this->environmentService->getBaseEnvironmentName();
    $this->syncFolder = $this->settings->get('config_sync_directory');

    $this->removeTemporaryDirectories();
  }

  /**
   * @inheritdoc
   **/
  public function getChangeList($import = TRUE) : array {
    $this->checkRequirements();
    $change_list = [];

    $temp_directory_path = $this->mergeConfigToTmpDirectory();

    if ($import) {
      $source_storage = new FileStorage($temp_directory_path);
      $active_storage = $this->configStorage;
    }
    else {
      $source_storage = $this->configStorage;
      $active_storage = new FileStorage($temp_directory_path);
    }
    $storage_comparer = new StorageComparer($source_storage, $active_storage, $this->configManager);
    $storage_comparer->createChangelist();

    if ($storage_comparer->createChangelist()->hasChanges()) {
      foreach ($storage_comparer->getAllCollectionNames() as $collection) {
        $change_list[$collection] = $storage_comparer->getChangelist(null, $collection);
      }
    }
    return $change_list;
  }

  /**
   * @inheritdoc
   **/
  public function import() : array {
    $this->checkRequirements();

    $temp_directory_path = $this->mergeConfigToTmpDirectory();
    $source_storage = new FileStorage($temp_directory_path);
    $storage_comparer = new StorageComparer($source_storage, $this->configStorage, $this->configManager);
    $storage_comparer->createChangelist();

    $change_list = [];
    if ($storage_comparer->createChangelist()->hasChanges()) {
      foreach ($storage_comparer->getAllCollectionNames() as $collection) {
        $change_list[$collection] = $storage_comparer->getChangelist(null, $collection);
      }
    }

    // TODO: simplify this when
    // https://www.drupal.org/project/drupal/issues/3123491 is fixed.
    $config_importer = new ConfigImporter(
      $storage_comparer,
      $this->eventDispatcher,
      $this->configManager,
      $this->persistentLockBackend,
      $this->typedConfigManager,
      $this->moduleHandler,
      $this->moduleInstaller,
      $this->themeHandler,
      $this->stringTranslationService,
      $this->moduleExtensionList
    );
    $config_importer->import();
    return $change_list;
  }

  /**
   * @inheritdoc
   **/
  public function export() : array {
    $this->checkRequirements();
    $current_environment_storage = NULL;

    $base_environment_path = $this->getEnvironmentFolderPath($this->baseEnvironmentName);
    $base_environment_storage = new FileStorage($base_environment_path);
    if ($this->currentEnvironmentName !== $this->baseEnvironmentName) {
      $current_environment_path = $this->getEnvironmentFolderPath($this->currentEnvironmentName);
      $current_environment_storage = new FileStorage($current_environment_path);
    }
    else {
      $current_environment_storage = $base_environment_storage;
    }
    $change_list = $this->getChangeList();
    $this->copyConfigToCorrectEnvironment($this->configStorage, $base_environment_storage, $current_environment_storage);
    return $change_list;
  }


  /**
   * Copies configuration objects from source storage to base or current environment storage.
   *
   * @param StorageInterface $source
   *   The source config storage service.
   * @param StorageInterface $base_environment
   *   The base environment config storage service.
   * @param StorageInterface $current_environment
   *   The current environment config storage service.
   */
  protected function copyConfigToCorrectEnvironment(
    StorageInterface $source,
    StorageInterface $base_environment,
    StorageInterface $current_environment) {

    // Make sure the source and destination are on the default collection.
    if ($source->getCollectionName() != StorageInterface::DEFAULT_COLLECTION) {
      $source = $source->createCollection(StorageInterface::DEFAULT_COLLECTION);
    }
    if ($base_environment->getCollectionName() != StorageInterface::DEFAULT_COLLECTION) {
      $base_environment = $base_environment->createCollection(StorageInterface::DEFAULT_COLLECTION);
    }
    if ($current_environment->getCollectionName() != StorageInterface::DEFAULT_COLLECTION) {
      $current_environment = $current_environment->createCollection(StorageInterface::DEFAULT_COLLECTION);
    }

    // Export all the configuration in their own folder.
    $currentEnvironmentConfigList = $this->environmentService->listConfiguration($this->currentEnvironmentName);
    foreach ($source->listAll() as $name) {
      if (in_array($name, $currentEnvironmentConfigList)) {
        $current_environment->write($name, $source->read($name));
      }
      else {
        $base_environment->write($name, $source->read($name));
      }
    }

    // Export configuration collections.
    foreach ($source->getAllCollectionNames() as $collection) {
      $source = $source->createCollection($collection);
      $base_environment = $base_environment->createCollection($collection);
      $current_environment = $current_environment->createCollection($collection);
      foreach ($source->listAll() as $name) {
        if (in_array($name, $currentEnvironmentConfigList)) {
          $current_environment->write($name, $source->read($name));
        }
        else {
          $base_environment->write($name, $source->read($name));
        }
      }
    }
  }

  /**
   * Merge configurations in a temporary directory.
   *
   * @return string
   *    Temporary directory path.
   */
  protected function mergeConfigToTmpDirectory() {
    // Copy base environment configurations to temp directory.
    $temp_directory_path = $this->createTemporaryDirectory();
    $base_environment_path = $this->getEnvironmentFolderPath($this->baseEnvironmentName);
    $base_environment = new FileStorage($base_environment_path);
    $temp_directory = new FileStorage($temp_directory_path);
    $currentEnvironmentConfigList = $this->environmentService->listConfiguration($this->currentEnvironmentName);
    foreach ($base_environment->listAll() as $name) {
      $temp_directory->write($name, $base_environment->read($name));
    }

    // Copy current environment configurations to temp directory.
    if ($this->currentEnvironmentName !== $this->baseEnvironmentName) {
      $current_environment_path = $this->getEnvironmentFolderPath($this->currentEnvironmentName);
      $current_environment = new FileStorage($current_environment_path);
      foreach ($current_environment->listAll() as $name) {
        if (in_array($name, $currentEnvironmentConfigList)) {
          $temp_directory->write($name, $current_environment->read($name));
        }
      }
    }
    return $temp_directory_path;
  }

  /**
   * Create environment path if not exists.
   *
   * @param $env_name
   *    Name of the environment.
   * @return string
   *    Path to the environment
   * @throws \Exception
   *    If there is an error at directory creation.
   */
  protected function getEnvironmentFolderPath($env_name) {
    $directory_path = $this->fileSystem->dirname($this->syncFolder);
    $directory_name = $this->fileSystem->basename($this->syncFolder);
    $environment_path = $directory_path. DIRECTORY_SEPARATOR . $directory_name. DIRECTORY_SEPARATOR .$env_name;

    if (!$this->fileSystem->realpath($environment_path)) {
      $this->fileSystem->mkdir($environment_path, NULL, TRUE);
      if (!$this->fileSystem->prepareDirectory($environment_path)) {
        throw new \Exception($this->t('Directory cannot be created or is not writable (@path)', [
          '@path' => $environment_path
        ]));
      }
    }
    return $environment_path;
  }


  /**
   * Create a temporary directory.
   *
   * @return string
   *    Directory path.
   * @throws \Exception
   *    If there is an error at directory creation.
   */
  protected function createTemporaryDirectory() {
    // Create a temporary directory to merge configurations.
    $temp_directory_path = $this->fileSystem->getTempDirectory().DIRECTORY_SEPARATOR.'env-sync-'.date('Y-m-d-H-i-s');
    $this->fileSystem->mkdir($temp_directory_path);
    if (!$this->fileSystem->prepareDirectory($temp_directory_path)) {
      throw new \Exception($this->t('Directory cannot be created or is not writable (@path)', [
        '@path' => $temp_directory_path
      ]));
    }
    return $temp_directory_path;
  }

  /**
   * Remove old temporary directories,
   *
   */
  protected function removeTemporaryDirectories() {
    $path_to_check = $this->fileSystem->getTempDirectory();

    // Remove all temporary directories.
    $cdir = scandir($path_to_check);
    foreach ($cdir as $key => $value) {
      if (!in_array($value, array(".",".."))) {
        if (is_dir($path_to_check . DIRECTORY_SEPARATOR . $value) && preg_match('#env-sync-[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}#', $value)){
          $this->fileSystem->deleteRecursive($path_to_check. DIRECTORY_SEPARATOR . $value);
        }
      }
    }
  }

  /**
   * Check if environments are set correctly.
   *
   * @throws \Exception
   */
  protected function checkRequirements() {
    if (empty($this->currentEnvironmentName)) {
      throw new \Exception($this->t('The current environment have to be define (eg: use command like @command).', [
        '@command' => 'drush env_sync:current my_env_name'
      ]));
    }

    if (empty($this->baseEnvironmentName)) {
      throw new \Exception($this->t('The base environment have to be define (eg: use command like @command).', [
        '@command' => 'drush env_sync:base my_env_name'
      ]));
    }
  }

}
