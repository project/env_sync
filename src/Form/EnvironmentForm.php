<?php

namespace Drupal\env_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\env_sync\Service\EnvironmentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EnvironmentForm.
 *
 * @package Drupal\env_sync\Form
 *
 */
class EnvironmentForm extends FormBase {

  /**
   * Environment synchronization service.
   *
   * @var \Drupal\env_sync\Service\EnvironmentInterface
   */
  protected $envSyncService;

  /**
   * Class constructor.
   */
  public function __construct(EnvironmentInterface $env_sync_service) {
    $this->envSyncService = $env_sync_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('env_sync.environment')
    );
  }

  /**
   * Env sync settings form Id.
   */
  const ENV_SYNC_FORM_ID = 'env_sync_environment_settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return self::ENV_SYNC_FORM_ID;
  }

  protected function getEnvironments($textarea_value) {
    $environments = [];
    $env_list = explode("\n", $textarea_value);
    foreach($env_list as $env_name) {
      $environments[] = trim($env_name);
    }
    return $environments;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $environments = $this->getEnvironments($form_state->getValue('environments_list'));
    $base = trim($form_state->getValue('base_environment'));
    $current = trim($form_state->getValue('current_environment'));

    if (!empty($environments)) {
      $environments_list = $this->envSyncService->list();

      // Remove deleted environments.
      foreach($environments_list as $env_name) {
        if (!in_array($env_name, $environments)) {
          $this->envSyncService->remove($env_name);
        }
      }

      try {
        // Add all new environments.
        foreach($environments as $env_name) {
          if (!empty($env_name) && !in_array($env_name, $environments_list)) {
            $this->envSyncService->add($env_name);
          }
        }
      }
      catch(\Exception $e) {
        $error = $this->t('Add environment: @message', [
          'message' => $e->getMessage()
        ]);
      }
    }

    // Set the base environment.
    if (!empty($base)) {
      if (!$this->envSyncService->isTheBaseEnvironment($base)) {
        $this->envSyncService->setBaseEnvironment($base);
      }
    }

    // Set the current environment.
    if (!empty($current)) {
      $this->envSyncService->setCurrentEnvironment($current);
    }
    else {
      $this->envSyncService->removeCurrentEnvironment();
    }

    if ($error) {
      \Drupal::messenger()->addWarning($error);
    }
    else {
      \Drupal::messenger()->addMessage($this->t('Environment configurations saved.'));
    }
  }

  /**
   * Get the list of defined environments.
   * 
   * @return Array
   *   List of environments.
   */
  protected function getEnvironmentsList() : array {
    $base_environment = $this->envSyncService->getBaseEnvironment();
    return $this->envSyncService->getAll(); 
  }

  protected function getOptions(array $environments) : array {
    $env_list = [];
    if (!empty($environments)) {
      $env_list[''] = '';
      foreach($environments as $env_key => $env_data) {
        $env_list[$env_key] = $env_data['name'];
      }
    }
    return $env_list;
  }

  /**
   * Defines the settings form for Humhub email entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $environments = $this->getEnvironmentsList();
    $options =  $this->getOptions($environments);
    $form_fields['fields'] = [
      'environments_list' => [
        '#title' => $this->t('List of environments'),
        '#type' => 'textarea',
        '#default_value' => implode("\n", array_keys($environments)),
        '#description' => $this->t('Put one environment name by line and use only alpha-numeric, "-" and "_" chars.')
      ]
    ];

    if (!empty($environments)) {
      $form_fields['fields']['base_environment'] = [
        '#title' => $this->t('Base environments'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $this->envSyncService->getBaseEnvironmentName(),
        '#description' => $this->t('Set an environment as base environment.')
      ];

      $form_fields['fields']['current_environment'] = [
        '#title' => $this->t('Current environments'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $this->envSyncService->getCurrentEnvironment(),
        '#description' => $this->t('Set an environment as current environment.')
      ];
    }
    $form_fields['actions'] = [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ]
    ];

    $form[self::ENV_SYNC_FORM_ID] = $form_fields;
    return $form;
  }

}
