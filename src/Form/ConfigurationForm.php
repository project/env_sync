<?php

namespace Drupal\env_sync\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\env_sync\Service\EnvironmentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigurationForm.
 *
 * @package Drupal\env_sync\Form
 *
 */
class ConfigurationForm extends FormBase {

  /**
   * Environment synchronization service.
   *
   * @var \Drupal\env_sync\Service\EnvironmentInterface
   */
  protected $envSyncService;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(EnvironmentInterface $env_sync_service, ConfigFactoryInterface $config_factory) {
    $this->envSyncService = $env_sync_service;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('env_sync.environment'),
      $container->get('config.factory')
    );
  }

  /**
   * Env sync settings form Id.
   */
  const ENV_SYNC_FORM_ID = 'env_sync_configuration_settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return self::ENV_SYNC_FORM_ID;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->envSyncService->list() as $env_name) {
      $configurations = $form_state->getValue($env_name, []);
      $current_conf_list = $this->envSyncService->listConfiguration($env_name);

      foreach($current_conf_list as $current_conf_name) {
        if (!in_array($current_conf_name, $configurations)) {
          $this->envSyncService->unsetConfiguration($current_conf_name, $env_name);
        }
      }

      foreach($configurations as $conf_name) {
        if (!in_array($conf_name, $current_conf_list)) {
          $this->envSyncService->setConfiguration($conf_name, $env_name);
        }
      }
    }
  }

  protected function getConfigurationsList() {
    $conf_list= [];
    $configurations = $this->configFactory->listAll();
    foreach($configurations as $conf_name) {
      $conf_list[$conf_name] = $conf_name;
    }
    return $conf_list;
  }

  /**
   * Defines the settings form for Humhub email entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_fields['fields'] = [
    ];

    foreach ($this->envSyncService->list() as $env_name) {
      $form_fields['fields'][$env_name] = [
        '#title' => $this->t('@env_name specifics configurations', ['@env_name' => ucfirst($env_name)]),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $this->getConfigurationsList(),
        '#default_value' => $this->envSyncService->listConfiguration($env_name),
      ];
    }

    $form_fields['actions'] = [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ]
    ];

    $form[self::ENV_SYNC_FORM_ID] = $form_fields;
    return $form;
  }

}
