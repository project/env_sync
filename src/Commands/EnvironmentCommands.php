<?php

namespace Drupal\env_sync\Commands;

use Drupal\env_sync\Service\EnvironmentInterface;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

/**
 * A Drush command file to manage Drupal environments.
 */
class EnvironmentCommands extends DrushCommands {

  /**
   * Environment synchronization service.
   *
   * @var \Drupal\env_sync\Service\EnvironmentInterface
   */
  protected $envSyncService;

  /**
   * Constructs a new EnvironmentCommands object.
   *
   */
  public function __construct(EnvironmentInterface $env_sync_service) {
    parent::__construct();

    $this->envSyncService = $env_sync_service;
  }

  /**
   * Create an environment.
   *
   * @param string $env_name
   *   The environment name.
   *
   * @bootstrap full
   * @command env_sync:add
   * @options base Whether or not to set as the base environment.
   * @usage env_sync:add prod
   * @usage env_sync:add prod --base
   */
  public function add($env_name, $options = ['base' => FALSE]) {
    $this->envSyncService->add($env_name);
    $this->logger()->success(dt('Environment "@env_name" has been added.', [
      '@env_name' => $env_name
    ]));
    if ($options['base'] === TRUE) {
      $this->setBase($env_name);
    }
  }

  /**
   * Remove an environment.
   *
   * @param string $env_name
   *   The environment name.
   *
   * @bootstrap full
   * @command env_sync:del
   * @usage env_sync:del prod
   */
  public function del($env_name) {
    if (!$this->io()->confirm(dt('Environment "@env_name" will be deleted', ['@env_name' => $env_name]))) {
      throw new UserAbortException();
    }
    $this->envSyncService->remove($env_name);
    $this->logger()->success(dt('Environment "@env_name" has been deleted.', [
      '@env_name' => $env_name
    ]));
  }

  /**
   * Set a base environment.
   *
   * @param string $env_name
   *   The environment name.
   *
   * @bootstrap full
   * @command env_sync:base
   * @usage env_sync:base prod
   */
  public function setBase($env_name) {
    $this->envSyncService->setBaseEnvironment($env_name);
    $this->logger()->success(dt('Environment "@env_name" is now the base environment.', [
      '@env_name' => $env_name
    ]));
  }

  /**
   * Define the current environment.
   *
   * @param string $env_name
   *   The environment name.
   *
   * @bootstrap full
   * @command env_sync:current
   * @options remove Used to remove from server state the defined current environment.
   * @usage env_sync:current prod
   * @usage env_sync:current --remove
   */
  public function setCurrent($env_name = '', $options = ['remove' => FALSE]) {
    if ($options['remove'] === TRUE) {
      $this->envSyncService->removeCurrentEnvironment();
      $this->logger()->success(dt('The current environment has been removed.'));
      $current_env_name = $this->envSyncService->getCurrentEnvironment();
      if (!empty($current_env_name)) {
        $this->logger()->notice(dt('The current environment is defined as the "@env_name" environment in settings.php.', [
          '@env_name' => $current_env_name
        ]));
      }
    }
    else {
      $this->envSyncService->setCurrentEnvironment($env_name);
      $this->logger()->success(dt('The current environment is defined as the "@env_name" environment.', [
        '@env_name' => $env_name
      ]));
    }
  }

  /**
   * List all available environments.
   *
   * @bootstrap full
   * @command env_sync:list
   * @usage env_sync:list
   */
  public function list() {
    $base_environment = $this->envSyncService->getBaseEnvironment();
    $env_list = $this->envSyncService->getAll();
    $current_env_name = $this->envSyncService->getCurrentEnvironment();
    $table = $this->environmentTable($env_list, $this->output(), $current_env_name);
    $table->render($env_list);
    if (empty($env_list)) {
      $this->io()->warning(dt('Environment list is empty.'));
    }
    else {
      if (empty($base_environment)) {
        $this->io()->warning(dt('* No base environment found. (use "env_sync:base" to set a base environment)'));
      }
    }
  }

  /**
   * Set a configuration in an environment.
   *
   * @param string $conf_name
   *   The configuration name.
   * @param string $env_name
   *   The environment name.
   *
   * @bootstrap full
   * @command env_sync:set_conf
   * @usage env_sync:set_conf block.block.bartik_branding prod
   */
  public function setConf($conf_name, $env_name) {
    if ($this->envSyncService->configurationExist($conf_name, $env_name)) {
      $this->logger()->info(dt('The configuration "@conf_name" already exists in "@env_name" environment.', [
        '@conf_name' => $conf_name,
        '@env_name' => $env_name
      ]));
    }
    else {
      if (!$this->envSyncService->setConfiguration($conf_name, $env_name)) {
        if (!$this->io()->confirm(dt('The configuration named "@conf_name" does not exist, confirm set in "@env_name" ?', [
          '@conf_name' => $conf_name,
          '@env_name' => $env_name
        ]))) {
          throw new UserAbortException();
        }
        $this->envSyncService->setConfiguration($conf_name, $env_name, TRUE);
      }
      $this->logger()->success(dt('The configuration named "@conf_name" has been set in "@env_name" environment.', [
        '@conf_name' => $conf_name,
        '@env_name' => $env_name
      ]));
    }
  }


  /**
   * Unset a configuration from an environment.
   *
   * @param string $conf_name
   *   The configuration name.
   * @param string $env_name
   *   The environment name.
   *
   * @bootstrap full
   * @command env_sync:unset_conf
   * @usage env_sync:unset_conf block.block.bartik_branding prod
   */
  public function unsetConf($conf_name, $env_name) {
    if ($this->envSyncService->unsetConfiguration($conf_name, $env_name)) {
      $this->logger()->success(dt('The configuration named "@conf_name" has been unset from "@env_name" environment.', [
        '@conf_name' => $conf_name,
        '@env_name' => $env_name
      ]));
    }
    else {
      $this->logger()->warning(dt('The configuration named "@conf_name" cannot be unset from "@env_name" environment.', [
        '@conf_name' => $conf_name,
        '@env_name' => $env_name
      ]));
    }
  }

  /**
   * Build a table of config changes.
   *
   * @param array $config_changes
   *   An array of changes keyed by collection.
   *
   * @return Table A Symfony table object.
   */
  public function environmentTable(array $env_list, OutputInterface $output, $current_env_name) {
    $rows = [];
    foreach ($env_list as $environment) {
      $definition = '';
      if ($environment[EnvironmentInterface::ENV_NAME] === $current_env_name) {
        $definition = 'Current';
        if ($environment[EnvironmentInterface::ENV_IS_BASE]) {
          $definition .= ' and base';
        }
      }
      else {
        if ($environment[EnvironmentInterface::ENV_IS_BASE]) {
          $definition = 'Base';
        }
      }

      if (!empty($definition)) {
        $definition .= ' environment';
      }

      $colour = "<fg=white;bg=black>";
      if ($environment[EnvironmentInterface::ENV_NAME] === $current_env_name) {
        $colour = '<fg=black;bg=green>';
      }
      $suffix = '</>';
      $rows[] = [
        $colour .$environment[EnvironmentInterface::ENV_NAME]. $suffix,
        '',
        $colour . dt($definition) . $suffix,
      ];

      natcasesort($environment[EnvironmentInterface::ENV_CONFIG]);
      foreach($environment[EnvironmentInterface::ENV_CONFIG] as $config) {
        $colour = '';
        $suffix = '';
        if ($environment[EnvironmentInterface::ENV_NAME] === $current_env_name) {
          $colour = '<fg=black;bg=green>';
          $suffix = '</>';
        }
        $rows[] = [
          '',
          $colour .$config. $suffix,
          '',
        ];
      }
    }
    $table = new Table($output);
    $table->setHeaders([dt('Name'), dt('Specific configurations'), dt('Definition')]);
    $table->addRows($rows);
    return $table;
  }

}