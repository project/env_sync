<?php

namespace Drupal\env_sync\Commands;

use Drupal\env_sync\Service\SynchronizationInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;
use Drush\Exceptions\UserAbortException;

/**
 * A Drush command file to synchronize Drupal environments.
 */
class SynchronizationCommands extends DrushCommands {

  /**
   * @var \Drupal\env_sync\Service\SynchronizationInterface
   */
  protected $SynchronizationService;

  /**
   * Constructs a new SynchronizationComandes object.
   */
  public function __construct(SynchronizationInterface $synchronization_service) {
    parent::__construct();

    $this->SynchronizationService = $synchronization_service;
  }

  /**
   * Import Drupal configurations linked to the current environment.
   *
   * @bootstrap full
   * @command env_sync:cim
   * @options y Whether or not skip the confirm mode.
   * @options s Whether or not to display the information text.
   * @usage env_sync:cim
   * @usage env_sync:cim --y
   * @usage env_sync:cim --y --s
   */
  public function import($options = ['y' => FALSE, 's' => FALSE]) {
    $change_list = $this->SynchronizationService->getChangeList(TRUE);
    if (empty($change_list)) {
      if (!$options['s']) {
        $this->io()->note(dt('No changes found in configurations.'));
      } 
    }
    else {
      if (!$options['s']) {
        $table = self::configChangesTable($change_list, $this->output(), 'From folder');
        $table->render();
        if (!$options['y'] && !$this->io()->confirm(dt('Configurations will be imported.'))) {
          throw new UserAbortException();
        }
      }
      $change_list = $this->SynchronizationService->import();
    }
  }

  /**
   * Export Drupal configurations linked to the current environment.
   *
   * @bootstrap full
   * @command env_sync:cex
   * @options y Whether or not skip the confirm mode.
   * @options s Whether or not to display the information text.
   * @usage env_sync:cex
   * @usage env_sync:cex --y
   * @usage env_sync:cex --y --s
   */
  public function export($options = ['y' => FALSE, 's' => FALSE]) {
    $change_list = $this->SynchronizationService->getChangeList(FALSE);
    if (empty($change_list)) {
      if (!$options['s']) {
        $this->io()->note(dt('No changes found in configurations.'));
      }
    }
    else {
      if (!$options['s']) {
        $table = self::configChangesTable($change_list, $this->output(), 'To folder');
        $table->render();
        if (!$options['y'] && !$this->io()->confirm(dt('Configurations will be exported.'))) {
          throw new UserAbortException();
        }
      }
      $change_list = $this->SynchronizationService->export();
    }

  }

  /**
   * Build a table of config changes.
   *
   * @param array $config_changes
   *   An array of changes keyed by collection.
   *
   * @return Table A Symfony table object.
   */
  public static function configChangesTable(array $config_changes, OutputInterface $output, $label_folder) {
    $rows = [];
    foreach ($config_changes as $collection => $changes) {
      foreach ($changes as $change => $configs) {
        switch ($change) {
          case 'delete':
            $colour = '<fg=white;bg=red>';
            break;
          case 'update':
            $colour = '<fg=black;bg=yellow>';
            break;
          case 'create':
            $colour = '<fg=white;bg=green>';
            break;
          default:
            $colour = "<fg=black;bg=cyan>";
            break;
        }
        $prefix = $colour;
        $suffix = '</>';
        foreach ($configs as $config) {
          $rows[] = [
            $collection,
            $config,
            $prefix . ucfirst($change) . $suffix,
          ];
        }
      }
    }
    $table = new Table($output);
    $table->setHeaders([dt('Collection'), dt('Config'), dt('Operation'), dt($label_folder)]);
    $table->addRows($rows);
    return $table;
  }

}