<?php

namespace Drupal\Tests\env_sync\Unit;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Site\Settings;
use Drupal\env_sync\Service\Environment;
use Drupal\env_sync\Service\EnvironmentInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests of the Environment Service.
 *
 * @coversDefaultClass \Drupal\env_sync\Service\Environment
 * @group env_sync
 */
class EnvironmentTest extends UnitTestCase {

  protected static $modules = ['env_sync'];

  protected $environment;

  protected $initialData = [
    'dev' => [
      EnvironmentInterface::ENV_NAME => 'dev',
      EnvironmentInterface::ENV_CONFIG => [],
      EnvironmentInterface::ENV_IS_BASE => TRUE,
    ],
    'integ' => [
      EnvironmentInterface::ENV_NAME => 'integ',
      EnvironmentInterface::ENV_CONFIG => [
        'block.block.bartik_branding'
      ],
      EnvironmentInterface::ENV_IS_BASE => FALSE,
    ]
  ];

  /**
   * Before a test method is run, setUp() is invoked.
   * Create new unit object.
   */
  public function setUp() : void {
    parent::setUp();

    $site_settings = [
      'env_sync.current_environment' => 'dev'
    ];
    $this->settings = new Settings($site_settings);


    $this->config_object = $this->prophesize(Config::class);
    $this->config_object->get('environment')
      ->willReturn($this->initialData);

    $this->configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $this->configFactory->getEditable(EnvironmentInterface::ENV_SYNC_ENVIRONMENT_CONFIG)
      ->willReturn($this->config_object->reveal());

    $this->stateService = $this->prophesize(StateInterface::class);
    $this->stateService->get(EnvironmentInterface::STATE_CURRENT_ENV, '')->willReturn('dev');

    $this->configManager = $this->prophesize(ConfigManagerInterface::class);

    $this->environment = new Environment($this->settings, $this->configFactory->reveal(), $this->stateService->reveal(), $this->configManager->reveal());
    }

  /**
   * @covers Drupal\env_sync\Service\Environment::add
   */
  public function testAdd() {
    $initial_data = $this->initialData;
    $initial_data['prod'] = [
      EnvironmentInterface::ENV_NAME => "prod",
      EnvironmentInterface::ENV_CONFIG => [],
      EnvironmentInterface::ENV_IS_BASE => false
    ];
    $this->config_object->set("environment", $initial_data)->shouldBeCalled();
    $this->config_object->save()->willReturn(TRUE);

    $this->environment->add('prod');

    $exception = FALSE;
    try {
      $this->environment->add('integ');
    }
    catch(\Exception $e) {
      $exception = TRUE;
    }
    $this->assertEquals($exception, TRUE, 'Add an existing environment should throw an exception.');

    $exception = FALSE;
    try {
      $this->environment->add('test_non_aplha_num&');
    }
    catch(\Exception $e) {
      $exception = TRUE;
    }
    $this->assertEquals($exception, TRUE, 'Add an environment with non alpanum chars in name should throw an exception.');

    $environments = $this->environment->list();
    $this->assertEquals(3, count($environments), 'Number of environment should be equal to 3.');
    $this->assertEquals(['dev', 'integ', 'prod'], $environments, 'List of environment is not the same.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::remove
   */
  public function testRemove() {
    $initial_data = $this->initialData;
    unset($initial_data['integ']);
    $this->config_object->set("environment",[
      "dev" => [
        EnvironmentInterface::ENV_NAME => "dev",
        EnvironmentInterface::ENV_CONFIG => [],
        EnvironmentInterface::ENV_IS_BASE => true
      ]
      ])->shouldBeCalled();
    $this->config_object->save()->willReturn(TRUE);

    $exception = FALSE;
    try {
      $this->environment->remove('integ', 'Remove integ environment');
    }
    catch(\Exception $e) {
      $exception = TRUE;
    }
    $this->assertEquals(FALSE, $exception, 'Remove an existing environment should not throw an exception.');

    $exception = FALSE;
    try {
      $this->environment->remove('test', 'Remove test environment');
    }
    catch(\Exception $e) {
      $exception = TRUE;
    }
    $this->assertEquals(TRUE, $exception, 'Remove an non existing environment should throw an exception.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::getBaseEnvironment
   */
  public function testGetBaseEnvironment() {
    $base_environment = $this->environment->getBaseEnvironment();

    $this->assertEquals('dev', $base_environment[EnvironmentInterface::ENV_NAME], 'Environment name should equal to dev.');
    $this->assertEquals(TRUE, $base_environment[EnvironmentInterface::ENV_IS_BASE], 'Environment is_base_environment should equal to TRUE.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::list
   */
  public function testList() {
    $environments = $this->environment->list();
    $this->assertEquals(2, count($environments), 'Number of environment list is not equal to 2.');
  }


  /**
   * @covers Drupal\env_sync\Service\Environment::environmentExists
   */
  public function testEnvironmentExists() {
    $this->assertEquals(TRUE, $this->environment->environmentExists('dev'), 'The dev environment should exists.');

    $exception = FALSE;
    try {
      $this->environment->environmentExists('test');
    }
    catch(\Exception $e){
      $exception = TRUE;
    }
    $this->assertEquals($exception, TRUE, 'Check existing of test environment should throw an exception.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::isTheBaseEnvironment
   */
  public function testIsTheBaseEnvironment() {
    $this->assertEquals(FALSE, $this->environment->isTheBaseEnvironment('integ'), 'Integ should not be the base environment.');
    $this->assertEquals(TRUE, $this->environment->isTheBaseEnvironment('dev'), 'Dev should be the base environment.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::setBaseEnvironment
   */
  public function testSetBaseEnvironment() {
    $initial_data = $this->initialData;
    $initial_data['dev'][EnvironmentInterface::ENV_IS_BASE] = FALSE;
    $initial_data['integ'][EnvironmentInterface::ENV_IS_BASE] = TRUE;

    $this->config_object->set("environment", $initial_data)->shouldBeCalled();
    $this->config_object->save()->willReturn(TRUE);

    $this->environment->setBaseEnvironment('integ');
    $base_environment = $this->environment->getBaseEnvironment();

    $this->assertEquals('integ', $base_environment[EnvironmentInterface::ENV_NAME], 'Environment name should equal to integ.');
    $this->assertEquals(TRUE, $base_environment[EnvironmentInterface::ENV_IS_BASE], 'Environment is_base_environment should equal to TRUE.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::setCurrentEnvironment
   */
  public function testSetCurrentEnvironment() {
    // If the current environment was defined in settings.php, it will override the state definition.

    $this->stateService->set(EnvironmentInterface::STATE_CURRENT_ENV, 'integ')->willReturn(TRUE);
    $this->environment->setCurrentEnvironment('integ');
    $current_environment_name = $this->environment->getCurrentEnvironment();

    $this->assertEquals('dev', $current_environment_name, 'The current environment name should equal to integ.');

    // If environment name is not existing.
    $exception = FALSE;
    try {
      $this->environment->setCurrentEnvironment('test');
    }
    catch(\Exception $e) {
      $exception = TRUE;
    }
    $this->assertEquals(TRUE, $exception, 'Set current environment to test should throw an exception.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::getAll
   */
  public function testGetAll() {
    $environments = $this->environment->getAll();
    $this->assertEquals($this->initialData, $environments, 'Should return all defined environments.');
  }

  /**
   * @covers Drupal\env_sync\Service\Environment::setConfiguration
   */
  public function testSetConfiguration() {
    // Set an existing configuration.
    $initial_data_with_config_breadcrumbs = $this->initialData;
    $initial_data_with_config_breadcrumbs['integ'][EnvironmentInterface::ENV_CONFIG][] = 'block.block.bartik_breadcrumbs';

    $this->config_object->set("environment", $initial_data_with_config_breadcrumbs)->shouldBeCalled();
    $this->config_object->save()->willReturn(TRUE);

    $this->configFactory->get('block.block.bartik_breadcrumbs')->willReturn($this->config_object->reveal());
    $this->config_object->isNew()->willReturn(FALSE);

    $this->environment->setConfiguration('block.block.bartik_breadcrumbs', 'integ');
    $environments = $this->environment->getAll();
    $this->assertEquals($initial_data_with_config_breadcrumbs, $environments, 'Should return all environments with the new configuration in integ.');

    // Try to set a non existing configuration.
    $initial_data_with_test_config = $initial_data_with_config_breadcrumbs;
    $initial_data_with_test_config['integ'][EnvironmentInterface::ENV_CONFIG][] = 'test';
    $this->config_object->set("environment", $initial_data_with_config_breadcrumbs)->shouldBeCalled();
    $this->configFactory->get('test')->willReturn($this->config_object->reveal());
    $this->config_object->isNew()->willReturn(TRUE);

    $this->environment->setConfiguration('test', 'integ');
    $environments = $this->environment->getAll();
    $this->assertEquals($initial_data_with_config_breadcrumbs, $environments, 'Should return all environments without the bad configuration "test"  in integ.');

    // Set a non existing configuration with force parameter.
    $this->config_object->set("environment", $initial_data_with_test_config)->shouldBeCalled();
    $this->configFactory->get('test')->willReturn($this->config_object->reveal());
    $this->config_object->isNew()->willReturn(TRUE);

    $this->environment->setConfiguration('test', 'integ', TRUE);
    $environments = $this->environment->getAll();
    $this->assertEquals($initial_data_with_test_config, $environments, 'Should return all environments with the bad configuration "test" in integ.');
  }


  /**
   * @covers Drupal\env_sync\Service\Environment::unsetConfiguration
   */
  public function testUnsetConfiguration() {
    // Unset an existing configuration.
    $initial_data = $this->initialData;
    $key = array_search('block.block.bartik_branding', $initial_data['integ'][EnvironmentInterface::ENV_CONFIG]);
    unset($initial_data['integ'][EnvironmentInterface::ENV_CONFIG][$key]);
    $this->config_object->set("environment", $initial_data)->shouldBeCalled();
    $this->config_object->save()->willReturn(TRUE);
    $saved = $this->environment->unsetConfiguration('block.block.bartik_branding', 'integ');
    $this->assertEquals(TRUE, $saved, 'The unset of configuration should return TRUE.');

    // Unset a non existing configuration.
    $saved = $this->environment->unsetConfiguration('block.block.bartik_branding', 'integ');
    $this->assertEquals(FALSE, $saved, 'The unset of configuration should return FALSE.');
  }

}